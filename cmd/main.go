package main

import (
	"fmt"
	"math/rand"
	"time"

	/*
		grob "github.com/MetalBlueberry/go-plotly/graph_objects"
		"github.com/MetalBlueberry/go-plotly/offline"
	*/
	"gitlab.com/malvido/polyfit/pkg/polynomial"
)

func gatherResults(polyChan chan polynomial.Polynomial, pending chan bool, xTrain, yTrain, xTest, yTest *polynomial.Vector) {
	jobsPending := 0
	for {
		select {
		case poly := <-polyChan:
			poly.PrintPolynomial()
			fmt.Printf("  Train Loss: %f\n", poly.Loss(xTrain, yTrain))
			fmt.Printf("   Test Loss: %f\n", poly.Loss(xTest, yTest))
			jobsPending--
			if jobsPending <= 0 {
				return
			}
		case <-pending:
			jobsPending++
		}
	}
}

func trainPoly(degree int, X, Y *polynomial.Vector, polyChan chan polynomial.Polynomial, pending chan bool) {
	pending <- true
	poly := polynomial.CreatePolyRandomCoefficients(degree, -1, 1)
	poly.SelfAdjust(X, Y, 100000)
	polyChan <- *poly
}

func main() {
	fmt.Println("The truth shall rise.")
	rand.Seed(time.Now().UnixNano())

	poly := polynomial.CreatePolyRandomCoefficients(5, -10, 10)
	poly.PrintPolynomial()

	coeffs := [3]float64{1, 2, 3}
	generator := polynomial.CreatePolyFromCoefficients(coeffs[:])
	generator.PrintPolynomial()

	X := polynomial.RandomVector(100, -2, 2)
	xTrain, xTest := X.Split(0.8)
	xTrain.Sort()
	xTest.Sort()
	fmt.Printf("Train size: %v\n", xTrain.Length)
	fmt.Printf(" Test size: %v\n", xTest.Length)

	Y := generator.Values(X)
	yTrain := generator.Values(xTrain)
	yTest := generator.Values(xTest)

	Y.StaticJitter(0.5)
	yTrain.StaticJitter(0.5)
	yTest.StaticJitter(0.5)

	fmt.Printf(" Noise Loss: %f\n", generator.Loss(X, Y))
	fmt.Printf("Random Loss: %f\n", poly.Loss(X, Y))

	polyChannel := make(chan polynomial.Polynomial)
	defer close(polyChannel)
	pendingChannel := make(chan bool)
	defer close(pendingChannel)

	for i := 1; i <= 8; i++ {
		go trainPoly(i, xTrain, yTrain, polyChannel, pendingChannel)
	}
	gatherResults(polyChannel, pendingChannel, xTrain, yTrain, xTest, yTest)
	/*
		gradients := poly.Gradients(X, Y)
		poly.Adjust(gradients, 0.0001)
		loss := poly.Loss(X, Y)
		fmt.Printf("Adjust Loss: %f\n", loss)

		trainLoss := poly.SelfAdjust(xTrain, yTrain, 100000, 0.000001, 0.0001)
		testLoss := poly.Loss(xTest, yTest)

		fmt.Printf(" Final Loss: %f\n", trainLoss)

		poly.PrintPolynomial()
		X.Sort()
		YP := poly.Values(X)
		graph := &grob.Fig{
			Data: grob.Traces{
				&grob.Scatter{
					Type: grob.TraceTypeScatter,
					X:    X.Values,
					Y:    YP.Values,
					Name: "Fit",
				},
				&grob.Scatter{
					Type: grob.TraceTypeScatter,
					X:    xTrain.Values,
					Y:    yTrain.Values,
					Mode: "markers",
					Name: fmt.Sprintf("Train Loss: %v", trainLoss),
				},
				&grob.Scatter{
					Type: grob.TraceTypeScatter,
					X:    xTest.Values,
					Y:    yTest.Values,
					Mode: "markers",
					Name: fmt.Sprintf("Test Loss: %v", testLoss),
				},
			},
			Layout: &grob.Layout{
				Title: &grob.LayoutTitle{
					Text: "Linear Regresion",
				},
			},
		}
		offline.Show(graph)
	*/
}
