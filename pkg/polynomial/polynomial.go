package polynomial

import (
	"fmt"
	"math"
	"math/rand"
)

type Polynomial struct {
	coefficients []float64
}

func CreatePolyFromCoefficients(coefficients []float64) *Polynomial {
	poly := Polynomial{coefficients: coefficients}
	return &poly
}

func CreatePolyRandomCoefficients(polyDegree int, randRange ...float64) *Polynomial {
	var min float64 = 0
	var max float64 = 1
	polyDegree++
	if len(randRange) >= 2 {
		if randRange[0] < randRange[1] {
			min = randRange[0]
			max = randRange[1]
		} else {
			min = randRange[1]
			max = randRange[0]
		}
	}
	if len(randRange) == 1 {
		if randRange[0] < 0 {
			min = randRange[0]
			max = 0
		} else {
			max = randRange[0]
		}
	}

	span := max - min

	coeffs := make([]float64, polyDegree)
	for i := 0; i < polyDegree; i++ {
		coeffs[i] = rand.Float64()*span + min
	}
	return CreatePolyFromCoefficients(coeffs)
}

func (poly *Polynomial) PrintPolynomial() {
	fmt.Printf("%f ", poly.coefficients[0])
	for i := 1; i < len(poly.coefficients); i++ {
		fmt.Printf(" %+f X ^ %d", poly.coefficients[i], i)
	}
	fmt.Println()
}

func (poly *Polynomial) Value(x float64) float64 {
	out := poly.coefficients[0]
	for i := 1; i < len(poly.coefficients); i++ {
		out += poly.coefficients[i] * math.Pow(x, float64(i))
	}
	return out
}

func (poly *Polynomial) Values(X *Vector) *Vector {
	Y := make([]float64, X.Length)
	for i := 0; i < X.Length; i++ {
		Y[i] = poly.Value(X.Values[i])
	}
	vector := Vector{Values: Y, Length: len(Y)}
	return &vector
}

func (poly *Polynomial) Loss(X, Y *Vector) float64 {
	var squares float64 = 0
	for i := 0; i < X.Length; i++ {
		squares += math.Pow(Y.Values[i]-poly.Value(X.Values[i]), 2)
	}
	return squares / float64(X.Length)
}

func (poly *Polynomial) Gradient(X, Y *Vector, coefficient int, delta ...float64) float64 {
	diff := 0.000001
	if len(delta) != 0 {
		diff = delta[0]
	}

	original := poly.coefficients[coefficient]
	poly.coefficients[coefficient] = original - diff
	loss0 := poly.Loss(X, Y)
	poly.coefficients[coefficient] = original + diff
	loss1 := poly.Loss(X, Y)
	poly.coefficients[coefficient] = original

	return (loss1 - loss0) / diff
}

func (poly *Polynomial) Gradients(X, Y *Vector, delta ...float64) *Vector {
	diff := 0.000001
	if len(delta) != 0 {
		diff = delta[0]
	}
	gradients := make([]float64, len(poly.coefficients))
	for i, _ := range poly.coefficients {
		gradients[i] = poly.Gradient(X, Y, i, diff)
	}

	vector := Vector{Values: gradients, Length: len(gradients)}
	return &vector
}

func (poly *Polynomial) Adjust(gradients *Vector, learningRate float64) {
	for i, coeff := range poly.coefficients {
		poly.coefficients[i] = coeff - learningRate*gradients.Values[i]
	}
}

func (poly *Polynomial) SelfAdjust(X, Y *Vector, maxIterations int, params ...float64) float64 {
	diff := 0.00001
	learningRate := 0.0001
	lossDiffLimit := 0.99999
	if len(params) > 2 {
		lossDiffLimit = params[2]
	}
	if len(params) > 1 {
		learningRate = params[1]
	}
	if len(params) > 0 {
		diff = params[0]
	}

	loss := poly.Loss(X, Y)
	for i := 0; i < maxIterations; i++ {
		gradients := poly.Gradients(X, Y, diff)
		poly.Adjust(gradients, learningRate)
		newLoss := poly.Loss(X, Y)
		if newLoss > loss {
			learningRate /= 10
			continue
		}
		if newLoss/loss > lossDiffLimit {
			return newLoss
		}
		loss = newLoss
	}
	return loss
}
