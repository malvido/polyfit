package polynomial

import (
	"math/rand"
	"sort"
)

type Vector struct {
	Values []float64
	Length int
}

func RandomVector(values int, min, max float64) *Vector {
	xRange := max - min
	X := make([]float64, values)
	for i := 0; i < values; i++ {
		X[i] = rand.Float64()*xRange + min
	}
	vector := Vector{Values: X, Length: len(X)}
	return &vector
}

func (vector *Vector) Sort() {
	sort.Float64s(vector.Values)
}

func (vector *Vector) Split(ratio float64) (*Vector, *Vector) {
	if ratio >= 1 || ratio <= 0 {
		return nil, nil
	}
	index := int(float64(vector.Length) * ratio)
	lowValues := make([]float64, index)
	copy(lowValues, vector.Values[:index])
	low := Vector{Values: lowValues, Length: len(lowValues)}

	highValues := make([]float64, vector.Length-index)
	copy(highValues, vector.Values[index:])
	high := Vector{Values: highValues, Length: len(highValues)}

	return &low, &high
}

func (vector *Vector) StaticJitter(amplitude float64) {
	for i, v := range vector.Values {
		vector.Values[i] = v + rand.Float64()*2*amplitude - amplitude
	}
}
